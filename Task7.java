import java.util.ArrayList;
import java.util.List;

public class Task7 {
    public List<String> list = new ArrayList<>();

    public void add(int ind,String val) {
        list.add(ind, val);
    }
    public void clear(){
        list.clear();
    }
    public String get(int ind){
        return list.get(ind);
    }
    public boolean isEmpty(){
        return list.isEmpty();
    }
    public int size(){
        return list.size();
    }
    public void removeind(int ind){
        list.remove(ind);
    }
    public void removeobj(Object obj){
        list.remove(obj);
    }
    public boolean contains(String obj){
        return list.contains(obj);
    }
}